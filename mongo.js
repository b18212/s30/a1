

	// Activity: (database: session30)

	//Aggregate to count the total number of items supplied by Red Farms Inc. ($count stage)
db.fruits.aggregate([
	{
		$match: {supplier: "Red Farms Inc."}
	},
	{
		$count: "itemsRedFarms"
	}

])

	//Aggregate to count the total number of items with price greater than 50. ($count stage)
db.fruits.aggregate([
db.fruits.aggregate([
	{
		$match: {price: {$gt: 50}}
	},
	{
		$count: "itemsGreaterThan50"
	}

])


	//Aggregate to get the average price of all fruits that are onSale per supplier.($group)
db.fruits.aggregate([
	{
		$match: {onSale: true}
	},
	{
		$group: {_id: "$supplier", avgPricePerSupplier: {$avg: "$price"}}
	}
])

	//Aggregate to get the highest price of fruits that are onSale per supplier. ($group)
	
db.fruits.aggregate([
	{
		$match: {onSale: true}
	},
	{
		$group: {_id: "$supplier", maxPricePerSupplier: {$max: "$price"}}
	}
])

	//Aggregate to get the lowest price of fruits that are onSale per supplier. ($group)

db.fruits.aggregate([
	{
		$match: {onSale: true}
	},
	{
		$group: {_id: "$supplier", minPricePerSupplier: {$min: "$price"}}
	}
])

	//save your query/commands in the mongo.js

